var myChart = document.getElementById('myChart').getContext('2d');
var myChart2 = document.getElementById('myChart2').getContext('2d');
var massPopChart = new Chart(myChart,{
	type:'doughnut',
	data:{
		labels:['University','Films','Books','Internet'],
		datasets:[{
			label:'Population',
			data:[
			50,
			20,
			20,
			10,
			],
			backgroundColor:[
				'rgba(255,99,20,0.6)',
				'rgba(80,120,50,0.6)',
				'rgba(180,44,255,0.6)',
				'rgba(100,200,50,0.6)'
			]
		}]
	},
	options:{
		title:{
			display:true,
			text:'Learning sources',
			fontSize:25
		}
	}
});

var massPopChart = new Chart(myChart2,{
	type:'bar',
	data:{
		labels:['Speaking','Reading','Listening'],
		datasets:[{
			label:'Difficulty-Scale',
			data:[
			8,
			4,
			6,
			0
			],
			backgroundColor:[
				'rgba(12,15,255,0.6)',
				'rgba(25,50,155,0.6)',
				'rgba(100,12,80,0.6)'
			]
		}]
	},
	options:{
		title:{
			display:true,
			text:'Hardest subject',
			fontSize:25
		},
		legend:{
			display:false
		}
	}
});

